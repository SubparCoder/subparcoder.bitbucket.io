var Lab2_8py =
[
    [ "count_isr", "Lab2_8py.html#a4d84c65f5ea73abc9a169db4d094186d", null ],
    [ "average_response", "Lab2_8py.html#ab6e3604049bf7e3aa61d3483b6ee7a44", null ],
    [ "blinky", "Lab2_8py.html#aa06bbd61dbe446eaf2ecd6cea6d68ac7", null ],
    [ "blinky_status", "Lab2_8py.html#a82b05c36ac18ad2b00d369fbd29e96a2", null ],
    [ "counter_status", "Lab2_8py.html#ac1350d56b0e14da3de65c91ff163a3b5", null ],
    [ "extint", "Lab2_8py.html#a91b7c18fc95ecec5c61bc3e4437d7c4c", null ],
    [ "random_time", "Lab2_8py.html#a812b0f4fb44d553ec366d04a17c47b0f", null ],
    [ "reaction_num", "Lab2_8py.html#a47981587867e8650d3cd1803655f867b", null ],
    [ "reaction_sum", "Lab2_8py.html#ab05e45cfe423f9eaf3a8c491da3a7c71", null ],
    [ "state", "Lab2_8py.html#a886efb8a774b805ac2e1d84cbcb9b3cd", null ],
    [ "tim", "Lab2_8py.html#a7bda6873ff5c1b9308f630d977b09e9a", null ]
];